﻿using System;

namespace SelectionStatments
{
    public static class Statements
    {
        public static void WriteLargestWithNestedIfElse(int first, int second, int third)
        {
            if (first > second)
            {
                if (first > third)
                {
                    Console.WriteLine($"Number {first} is the largest");
                }
                else
                {
                    Console.WriteLine($"Number {third} is the largest");
                }
            }
            else
            {
                if (second > third)
                {
                    Console.WriteLine($"Number {second} is the largest");
                }
                else
                {
                    Console.WriteLine($"Number {third} is the largest");
                }
            }
        }

        public static void WriteLargestWithIfElseAndTernaryOperator(int first, int second, int third)
        {
            if (first > second)
            {
                Console.WriteLine($"Number {(first > third ? first : third)} is the largest");
            }
            else
            {
                Console.WriteLine($"Number {(second > third ? second : third)} is the largest");
            }
        }

        public static void WriteLargestWithIfElseAndConditionLogicalOperators(int first, int second, int third)
        {
            if (first > second && first > third)
            {
                Console.WriteLine($"Number {first} is the largest");
            }
            else
            {
                if (second > first && second > third)
                {
                    Console.WriteLine($"Number {second} is the largest");
                }
                else
                {
                    Console.WriteLine($"Number {third} is the largest");
                }
            }
        }

        public static void HowOldAreYouReactionWithCascadedIfElse(int userAge)
        {
            if (userAge >= 65)
            {
                Console.WriteLine("Enjoy your retirement!");
            }
            else
            {
                if (userAge >= 21)
                {
                    Console.WriteLine("Fancy an alcoholic beverage?");
                }
                else
                {
                    if (userAge >= 18)
                    {
                        Console.WriteLine("You're old enough to drive.");
                    }
                    else
                    {
                        Console.WriteLine("You are too young to drive, drink, or retire.");
                    }
                }
            }
        }

        public static void WriteInformationAboutDailyDownloadsWithCascadedIfElse(int countOfDailyDownloads)
        {
            if (countOfDailyDownloads <= 0)
            {
                Console.WriteLine("No downloads.");
            }

            if (countOfDailyDownloads >= 1 && countOfDailyDownloads < 100)
            {
                Console.WriteLine("Daily downloads: 1-100.");
            }

            if (countOfDailyDownloads >= 100 && countOfDailyDownloads < 1000)
            {
                Console.WriteLine("Daily downloads: 100-1,000.");
            }

            if (countOfDailyDownloads >= 1000 && countOfDailyDownloads < 10000)
            {
                Console.WriteLine("Daily downloads: 1,000-10,000.");
            }

            if (countOfDailyDownloads >= 10000 && countOfDailyDownloads < 100000)
            {
                Console.WriteLine("Daily downloads: 10,000-100,000.");
            }

            if (countOfDailyDownloads >= 100000)
            {
                Console.WriteLine("Daily downloads: 100,000+.");
            }
        }

        public static void WriteTheInformationAboutDayWithIfElse(DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == DayOfWeek.Monday)
            {
                Console.WriteLine("The first day of the work week.");
            }

            if (dayOfWeek == DayOfWeek.Tuesday || dayOfWeek == DayOfWeek.Wednesday || dayOfWeek == DayOfWeek.Thursday)
            {
                Console.WriteLine("The middle of the work week.");
            }

            if (dayOfWeek == DayOfWeek.Friday)
            {
                Console.WriteLine("The last day of the work week.");
            }

            if (dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                Console.WriteLine("The weekend.");
            }
        }

        public static void WriteTheInformationAboutDayWithSwitchStatement(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    Console.WriteLine("The first day of the work week.");
                    break;
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    Console.WriteLine("The middle of the work week.");
                    break;
                case DayOfWeek.Friday:
                    Console.WriteLine("The last day of the work week.");
                    break;
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    Console.WriteLine("The weekend.");
                    break;
            }
        }

        public static string GetTypeOfIntegerWithCascadedIfElse(object arg)
        {
            if (arg is null)
            {
                return null;
            }

            if (arg is sbyte)
            {
                return arg.ToString() + " is sbyte.";
            }

            if (arg is byte)
            {
                return arg.ToString() + " is byte.";
            }

            if (arg is short)
            {
                return arg.ToString() + " is short.";
            }

            if (arg is int)
            {
                return arg.ToString() + " is int.";
            }

            if (arg is long)
            {
                return arg.ToString() + " is long.";
            }

            if (arg is ushort)
            {
                return arg.ToString() + " is ushort.";
            }

            if (arg is uint)
            {
                return arg.ToString() + " is uint.";
            }

            if (arg is ulong)
            {
                return arg.ToString() + " is ulong.";
            }
            else
            {
                return arg.ToString() + " is not integer.";
            }
        }

        public static string GetTypeOfIntegerWithSwitchStatement(object arg)
        {
            if (arg is null)
            {
                return null;
            }

            switch (arg)
            {
                case sbyte _:
                    return arg.ToString() + " is sbyte.";
                case byte _:
                    return arg.ToString() + " is byte.";
                case short _:
                    return arg.ToString() + " is short.";
                case int _:
                    return arg.ToString() + " is int.";
                case long _:
                    return arg.ToString() + " is long.";
                case ushort _:
                    return arg.ToString() + " is ushort.";
                case uint _:
                    return arg.ToString() + " is uint.";
                case ulong _:
                    return arg.ToString() + " is ulong.";
                default:
                    return arg.ToString() + " is not integer.";
            }
        }

        public static string GetTypeOfIntegerWithSwitchExpression(object arg)
        {
            if (arg is null)
            {
                return null;
            }

            return arg switch
            {
                sbyte _ => arg.ToString() + " is sbyte.",
                byte _ => arg.ToString() + " is byte.",
                short _ => arg.ToString() + " is short.",
                int _ => arg.ToString() + " is int.",
                long _ => arg.ToString() + " is long.",
                ushort _ => arg.ToString() + " is ushort.",
                uint _ => arg.ToString() + " is uint.",
                ulong _ => arg.ToString() + " is ulong.",
                _ => arg.ToString() + " is not integer.",
            };
        }

        public static void WriteTheInformationAboutSeasonsWithSwitchStatement(Month month)
        {
            switch (month)
            {
                case Month.December:
                case Month.January:
                case Month.February:
                    Console.WriteLine("It's winter now.");
                    break;
                case Month.March:
                case Month.April:
                case Month.May:
                    Console.WriteLine("It's spring now.");
                    break;
                case Month.June:
                case Month.July:
                case Month.August:
                    Console.WriteLine("It's summer now.");
                    break;
                case Month.September:
                case Month.October:
                case Month.November:
                    Console.WriteLine("It's autumn now.");
                    break;
                default:
                    Console.WriteLine("Sorry, the month was entered incorrectly.");
                    break;
            }
        }

        public static byte GetLengthWithCascadedIfElse(int number)
        {
            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 1)
            {
                return 1;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 2)
            {
                return 2;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 3)
            {
                return 3;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 4)
            {
                return 4;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 5)
            {
                return 5;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 6)
            {
                return 6;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 7)
            {
                return 7;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 8)
            {
                return 8;
            }

            if ((byte)Math.Log10(Math.Abs((long)number)) + 1 == 9)
            {
                return 9;
            }
            else
            {
                return 10;
            }
        }

        public static byte GetLengthWithSwitchExpression(int number)
        {
            return ((byte)Math.Log10(Math.Abs((long)number)) + 1) switch
            {
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 6,
                7 => 7,
                8 => 8,
                9 => 9,
                _ => 10,
            };
        }

        public static Month? GetMonthWithCascadedIfElse(int month)
        {
            if (month == 1)
            {
                return Month.January;
            }

            if (month == 2)
            {
                return Month.February;
            }

            if (month == 3)
            {
                return Month.March;
            }

            if (month == 4)
            {
                return Month.April;
            }

            if (month == 5)
            {
                return Month.May;
            }

            if (month == 6)
            {
                return Month.June;
            }

            if (month == 7)
            {
                return Month.July;
            }

            if (month == 8)
            {
                return Month.August;
            }

            if (month == 9)
            {
                return Month.September;
            }

            if (month == 10)
            {
                return Month.October;
            }

            if (month == 11)
            {
                return Month.November;
            }

            if (month == 12)
            {
                return Month.December;
            }
            else
            {
                return null;
            }
        }

        public static Month? GetMonthWithSwitchStatement(int month)
        {
            switch (month)
            {
                case 1:
                    return Month.January;
                case 2:
                    return Month.February;
                case 3:
                    return Month.March;
                case 4:
                    return Month.April;
                case 5:
                    return Month.May;
                case 6:
                    return Month.June;
                case 7:
                    return Month.July;
                case 8:
                    return Month.August;
                case 9:
                    return Month.September;
                case 10:
                    return Month.October;
                case 11:
                    return Month.November;
                case 12:
                    return Month.December;
                default:
                    return null;
            }
        }

        public static Month? GetMonthWithSwitchExpression(int month)
        {
            return month switch
            {
                1 => Month.January,
                2 => Month.February,
                3 => Month.March,
                4 => Month.April,
                5 => Month.May,
                6 => Month.June,
                7 => Month.July,
                8 => Month.August,
                9 => Month.September,
                10 => Month.October,
                11 => Month.November,
                12 => Month.December,
                _ => null,
            };
        }
    }
}
